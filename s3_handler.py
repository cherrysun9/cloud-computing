import ast
import boto3
import logging
import os
import sys
import traceback
import time

LOG_FILE_NAME = 'output.log'

REGION = 'us-west-2'

class S3Handler:
    """S3 handler."""

    def __init__(self):
        self.client = boto3.client('s3')

        logging.basicConfig(filename=LOG_FILE_NAME,
                            level=logging.DEBUG, filemode='w',
                            format='%(asctime)s %(message)s',
                            datefmt='%m/%d/%Y %I:%M:%S %p')
        self.logger = logging.getLogger("S3Handler")

    def help(self):
        print("Supported Commands:")
        print("1. createdir <bucket_name>")
        print("2. upload <source_file_name> <bucket_name> [<dest_object_name>]")
        print("3. download <dest_object_name> <bucket_name> [<source_file_name>]")
        print("4. delete <dest_object_name> <bucket_name>")
        print("5. deletedir <bucket_name>")
        print("6. find <file_extension> [<bucket_name>] -- e.g.: 1. find txt  2. find txt bucket1 --")
        print("7. listdir [<bucket_name>]")
    
    def _error_messages(self, issue):
        error_message_dict = {}
        error_message_dict['operation_not_permitted'] = 'Not authorized to access resource.'
        error_message_dict['incorrect_parameter_number'] = 'Incorrect number of parameters provided'
        error_message_dict['not_implemented'] = 'Functionality not implemented yet!'
        error_message_dict['bucket_name_exists'] = 'Directory already exists.'
        error_message_dict['bucket_name_empty'] = 'Directory name cannot be empty.'
        error_message_dict['non_empty_bucket'] = 'Directory is not empty.'
        error_message_dict['missing_source_file'] = 'Source file cannot be found.'
        error_message_dict['non_existent_bucket'] = 'Directory does not exist.'
        error_message_dict['non_existent_object'] = 'Destination File does not exist.'
        error_message_dict['unknown_error'] = 'Something was not correct with the request. Try again.'

        if issue:
            return error_message_dict[issue]
        else:
            return error_message['unknown_error']

    def _get_file_extension(self, file_name):
        if os.path.exists(file_name):
            return os.path.splitext(file_name)

    def _get(self, bucket_name):
        response = ''
        try:
            response = self.client.head_bucket(Bucket=bucket_name)
        except Exception as e:
            response_code = e.response['Error']['Code']
            if response_code == '404':
                return False
            elif response_code == '200':
                return True
            else:
                raise e
        if response['ResponseMetadata']['HTTPStatusCode'] == 200:
            return True
        else:
            return False

    def createdir(self, bucket_name):
        if not bucket_name:
            return self._error_messages('bucket_name_empty')

        try:
            if self._get(bucket_name):
                return self._error_messages('bucket_name_exists')
            self.client.create_bucket(Bucket=bucket_name,
                                      CreateBucketConfiguration={'LocationConstraint': REGION})
        except Exception as e:
            print(e)
            raise e

        # Success response
        operation_successful = ('Directory %s created.' % bucket_name)
        return operation_successful

    def listdir(self, bucket_name):
        response = self.client.list_buckets()
        if not bucket_name:
            buckets_list = []
            for bucket in response['Buckets']:
                buckets_list.append(bucket["Name"])
            return buckets_list
 
        if self._get(bucket_name):
            s3 = boto3.resource('s3')
            bucket = s3.Bucket(bucket_name)
            bucket_obj = []
            summary = self.client.list_objects(Bucket=bucket_name)
            if 'Contents' not in summary.keys():
                return bucket_obj
            for key in self.client.list_objects(Bucket=bucket_name)['Contents']:
                bucket_obj.append(key['Key'])
            return bucket_obj

        return self._error_messages('non_existent_bucket')
        # If bucket_name is provided, check that bucket exits.
        
        # If bucket_name is empty then display the names of all the buckets
        
        # If bucket_name is provided then display the names of all objects in the bucket

    def upload(self, source_file_name, bucket_name, dest_object_name=''):

        if not self._get(bucket_name):
            return self._error_messages('non_existent_bucket')
        if not os.path.isfile(source_file_name):
            return self._error_messages('missing_source_file')

        if not dest_object_name:
            dest_object_name = source_file_name

        extension = self._get_file_extension(source_file_name)
        self.client.upload_file(source_file_name, bucket_name, dest_object_name, 
            ExtraArgs = {'Metadata': {'file-extension': extension[1][1:] }})

        # 1. Parameter Validation
        #    - source_file_name exits in current directory
        #    - bucket_name exists
        # 2. If dest_object_name is not specified then use the source_file_name as dest_object_name

        # 3. SDK call
        #    - When uploading the source_file_name and add it to object's meta-data
        #    - Use self._get_file_extension() method to get the extension of the file.

        # Success response
        operation_successful = ('File %s uploaded to directory %s.' % (source_file_name, bucket_name))
        return operation_successful


    def download(self, dest_object_name, bucket_name, source_file_name=''):
        if not self._get(bucket_name):
            return self._error_messages('non_existent_bucket')

        try:
            obj = self.client.head_object(Bucket=bucket_name, Key=dest_object_name)
        except:
            return self._error_messages('non_existent_object')

        if not source_file_name:
            source_file_name = dest_object_name

        if os.path.isfile(source_file_name):
            current_time_stamp_in_millis = round(time.time()*1000)
            name = self._get_file_extension(source_file_name)
            source_file_name = name[0] + str(current_time_stamp_in_millis) + name[1]

        obj = self.client.head_object(Bucket=bucket_name, Key=dest_object_name)
        self.client.download_file(bucket_name, dest_object_name, source_file_name)

        # if source_file_name is not specified then use the dest_object_name as the source_file_name
        # If the current directory already contains a file with source_file_name then move it as a backup
        # with following format: <source_file_name.bak.current_time_stamp_in_millis>
        

        # Parameter Validation
        
        # SDK Callh

        # Success response
        operation_successful = ('File %s downloaded from directory %s.' % (dest_object_name, bucket_name))
        return operation_successful


    def delete(self, dest_object_name, bucket_name):
        if not self._get(bucket_name):
            return self._error_messages('non_existent_bucket')
        try:
            self.client.head_object(Bucket=bucket_name, Key=dest_object_name)
        except:
            return self._error_messages('non_existent_object')

        s3 = boto3.resource("s3")
        obj = s3.Object(bucket_name, dest_object_name)
        obj.delete()
        operation_successful = ('File %s deleted from directory %s.' % (dest_object_name, bucket_name))
        return operation_successful


    def deletedir(self, bucket_name):
        if not self._get(bucket_name):
            return self._error_messages('non_existent_bucket')
        s3 = boto3.resource('s3')
        bucket = s3.Bucket(bucket_name)
        count = 0
        for each in bucket.objects.all():
            count+=1
        if(count != 0):
            return self._error_messages('non_empty_bucket')
        bucket.delete()

        # Delete the bucket only if it is empty
        # Success response
        operation_successful = ("Deleted directory %s." % bucket_name)
        return operation_successful


    def find(self, file_extension, bucket_name=''):
        # Return object names that match the given file extension
        name_list = []
        s3 = boto3.resource('s3')
        if not bucket_name:
            response = self.client.list_buckets()
            for bucket in response['Buckets']:
                bucket_name = bucket["Name"]
                mybucket = s3.Bucket(bucket_name)
                for each in mybucket.objects.all():
                    key = each.key
                    obj = self.client.head_object(Bucket=bucket_name, Key=key)
                    if obj['Metadata']['file-extension'] == file_extension:
                        name_list.append(key)
        elif self._get(bucket_name):    
            mybucket = s3.Bucket(bucket_name)
            for each in mybucket.objects.all():
                key = each.key
                obj = self.client.head_object(Bucket=bucket_name, Key=key)
                if obj['Metadata']['file-extension'] == file_extension:
                    name_list.append(key)
        else:
            return self._error_messages('non_existent_bucket')

        #obj = self.client.head_object(Bucket=bucket_name, Key='testdata.txt')
        #print(obj['Metadata']['file-extension'])
        
        # If bucket_name is specified then search for objects in that bucket.
        # If bucket_name is empty then search all buckets
        if len(name_list) == 0:
            return 'No Files Found'
        
        return name_list


    def dispatch(self, command_string):
        parts = command_string.split(" ")
        response = ''

        if parts[0] == 'createdir':
            # Figure out bucket_name from command_string
            if len(parts) > 1:
                bucket_name = parts[1]
                response = self.createdir(bucket_name)
            else:
                # Parameter Validation
                # - Bucket name is not empty
                response = self._error_messages('bucket_name_empty')
        elif parts[0] == 'upload':
            # Figure out parameters from command_string
            # source_file_name and bucket_name are compulsory; dest_object_name is optional
            # Use self._error_messages['incorrect_parameter_number'] if number of parameters is less
            # than number of compulsory parameters
            if len(parts) < 3:
                response = self._error_messages('incorrect_parameter_number')
            else:
                source_file_name = parts[1]
                bucket_name = parts[2]
                if len(parts) == 4:
                    dest_object_name = parts[3]
                else:
                    dest_object_name = ''
                response = self.upload(source_file_name, bucket_name, dest_object_name)

        elif parts[0] == 'download':
            # Figure out parameters from command_string
            # dest_object_name and bucket_name are compulsory; source_file_name is optional
            # Use self._error_messages['incorrect_parameter_number'] if number of parameters is less
            # than number of compulsory parameters
            if len(parts)< 3:
                response = self._error_messages('incorrect_parameter_number')
            else:
                if(len(parts) == 3):
                    source_file_name = ''
                else:
                    source_file_name = parts[3]
                bucket_name = parts[2]
                dest_object_name = parts[1]
                response = self.download(dest_object_name, bucket_name, source_file_name)
        elif parts[0] == 'delete':
            if len(parts)< 3:
                response = self._error_messages('incorrect_parameter_number')
            else:
                dest_object_name = parts[1]
                bucket_name = parts[2]
                response = self.delete(dest_object_name, bucket_name)
        elif parts[0] == 'deletedir':
            if len(parts)>1:
                bucket_name = parts[1]
                response = self.deletedir(bucket_name)
            else:
                response = self._error_messages('bucket_name_empty')
        elif parts[0] == 'find':
            if len(parts) < 2:
                response = self._error_messages('incorrect_parameter_number')
            else:
                file_extension = parts[1]
                if len(parts) == 3:
                    bucket_name = parts[2]
                else:
                    bucket_name = ''
                response = self.find(file_extension, bucket_name)
        elif parts[0] == 'listdir':
            if len(parts)>1:
                bucket_name = parts[1]
                response = self.listdir(bucket_name)
            else:
                response = self.listdir('')
        else:
            response = "Command not recognized."
        return response


def main():
    s3_handler = S3Handler()
    while True:
        try:
            command_string = ''
            if sys.version_info[0] < 3:
                command_string = raw_input("Enter command ('help' to see all commands, 'exit' to quit)>")
            else:
                command_string = input("Enter command ('help' to see all commands, 'exit' to quit)>")
    
            # Remove multiple whitespaces, if they exist
            command_string = " ".join(command_string.split())
            
            if command_string == 'exit':
                print("Good bye!")
                exit()
            elif command_string == 'help':
                s3_handler.help()
            else:
                response = s3_handler.dispatch(command_string)
                print(response)
        except Exception as e:
            print(e)

if __name__ == '__main__':
    main()
